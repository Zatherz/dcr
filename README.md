[![docs](https://img.shields.io/badge/docs-latest-green.svg?style=flat-square)](https://zatherz.gitlab.io/dcr/)

# DCR

DCR is a [Discord](https://discordapp.com/) library for
[Crystal](https://crystal-lang.org/), aimed to be easy to use and have an object
oriented interface.

**DCR is a fork of [discordcr](https://github.com/meew0/discordcr), which is a
Discord library aimed at minimalism.**

User accounts are not officially supported by DCR.

## Installation

Add this to your application's `shard.yml`:

```yaml
dependencies:
  dcr:
    gitlab: zatherz/dcr
```

## Usage

An example bot which responds to `!ping` commands can be found
[here](https://gitlab.com/zatherz/dcr/blob/master/examples/ping.cr).

A multi-command example can be found [here](https://gitlab.com/zatherz/dcr/blob/master/examples/multicommand.cr).

## Errors

Errors are divided into 3 "categories":

* `CodeError` [Discord error] - raised when a REST request to Discord errors with a code and a descriptive message

* `StatusError` [HTTP error] - raised when a REST request fails in a general way

* `DCRError` [DCR error] - extended by dcr-specific errors, usually raised in various situations of DCR misuse
  (e.g. `not_this_user.username = "abc"`)

## Library structure

A short overview of library structure:

* The `Client` class includes the `REST` module, which handles the
  REST Discord API, while `Client` handles the gateway/websocket
  connection. With this in mind, `Client` isn't ideal for gateway
  usage because of the complete absence of any sort of caching.

* `CachedClient` extends `Client`, and it's what you want to use for
  bots. It implements a cache on top of `Client`, with essentially
  the same API.

* The payloads contain methods which simplify the usage of the library.
  For example, instead of using this lengthy snippet to get the `GuildMember`
  behind a message:

  ```
  user = msg.author
  channel = msg.channel
  raise "Not a guild text channel" if !channel.is_a? GuildTextChannel
  member = client.get_guild_member user_id: user.id, guild_id: channel.guild_id
  ```

  You can use this simple, single method:

  ```
  member = msg.member!
  ```

  This is the biggest difference compared to discordcr.

Library documentation is available at https://zatherz.gitlab.io/dcr/

## Contributors

#### [discordcr]
- [meew0](https://github.com/meew0) - creator, maintainer
- [RX14](https://github.com/RX14) - Crystal expert, maintainer

#### [dcr]
- [Zatherz](https://gitlab.com/u/zatherz) - creator, maintainer
