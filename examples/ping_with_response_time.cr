# This example is nearly the same as the normal ping example, but rather than simply
# responding with "Pong!", it also responds with the time it took to send the message.

require "../src/dcr"

# Set the DCR_TOKEN environment variable before running this script, or just change this:
client = Discord::CachedClient.new "Bot #{ENV["DCR_TOKEN"]}"

client.on_message_create do |msg|
  if msg.content.starts_with? "!ping"
    # We first respond with a message...
    m = msg.respond "#{msg.author} Pong!"
    # and then we check how long it took to send the message by comparing it to the current time
    time = Time.utc_now - m.timestamp
    m.edit("#{msg.author} Pong! Time taken: #{time.total_milliseconds} ms.")
  end
end

client.run
