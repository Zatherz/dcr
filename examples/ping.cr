# This simple example bot replies to every "!ping" message with "Pong!".

require "../src/dcr"

# Set the DCR_TOKEN environment variable before running this script, or just change this:
client = Discord::CachedClient.new "Bot #{ENV["DCR_TOKEN"]}"

client.on_message_create do |msg|
  msg.respond "Pong!" if msg.content.starts_with? "!ping"
end

client.run
