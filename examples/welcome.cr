# This simple example bot creates a message whenever a new user joins the server

require "../src/dcr"

# Set the DCR_TOKEN environment variable before running this script, or just change this:
client = Discord::CachedClient.new "Bot #{ENV["DCR_TOKEN"]}"

client.on_guild_member_add do |member|
  puts member
  puts member.guild

  member.guild.default_text_channel.send "Please welcome #{member} to #{member.guild}"
end

client.run
