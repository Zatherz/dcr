require "./voice"

module Discord
  # Common mixin for `Guild` and `Gateway::GuildCreatePayload`.
  module GuildCommon
    include Payload::WithClientAccess

    field id : UInt64, converter: SnowflakeConverter
    field name : String
    field icon : String? = nil
    field splash : String? = nil
    field owner_id : UInt64, converter: SnowflakeConverter
    field region : String
    field afk_channel_id : UInt64? = nil, converter: MaybeSnowflakeConverter
    field afk_timeout : Int32? = nil
    field embed_enabled? : Bool = false, key: "embed_enabled"
    field embed_channel_id : UInt64? = nil, converter: MaybeSnowflakeConverter
    field verification_level : Guild::VerificationLevel
    field emojis : Array(Emoji)
    field features : Array(String)

    def to_s(io)
      io << name
    end

    def roles
      @client.get_guild_roles id
    end

    class RoleDoesntExistError < DCRError
      getter name : String

      def initialize(@name)
        super "Role with name '#{@name}' doesn't exist in this guild."
      end
    end

    # Looks up a role with the *name*, returns `nil` if not found.
    def role(name : String)
      roles.find { |r| r.name == name }
    end

    # Looks up a role with the *name*, raises `RoleDoesntExist` if not found.
    def role!(name : String)
      role(name) || raise RoleDoesntExistError.new name
    end

    def default_text_channel
      @client.get_channel(@id).as(GuildTextChannel)
    end

    {% unless flag?(:murica) %}
      def emoji
        emojis
      end
    {% end %}

    class EmojiDoesntExistError < DCRError
      getter guild : Guild
      getter emoji_name : String

      def initialize(@guild, @emoji_name)
        super "The '#{@guild}' guild does not have a :#{@emoji_name}: emoji."
      end
    end

    # Attempts to find a custom emoji from this server by name, returns
    # `nil` if not found.
    def emoji(name : String)
      @emojis.find { |e| e.name == name }
    end

    # Attempts to find a custom emoji from this server by name, raises
    # `EmojiDoesntExistError` if not found.
    def emoji!(name : String)
      emoji(name) || raise EmojiDoesntExistError.new(self, name)
    end

    # Leave this guild as the client.
    def leave
      @client.leave_guild @id
    end

    def embed
      @client.get_guild_embed @id
    end

    def voice_regions
      @client.get_guild_voice_regions @id
    end

    def integrations
      @client.get_guild_integrations @id
    end

    def integrate(integration : Integration)
      @client.create_guild_integration @id, integration.type, integration.id
    end

    def disintegrate(integration : Integration)
      @client.delete_guild_integration @id, integration.id
    end

    def modify_integration(integration : Integration,
                           expire_behaviour : UInt8,
                           expire_grace_period : Int32,
                           enable_emoticons : Bool)
      @cilent.modify_guild_integration(
        @id, integration.id, expire_behaviour,
        expire_grace_period, enable_emoticons
      )
    end

    def sync_integration(integration : Integration)
      @client.sync_guild_integration @id, integration.id
    end

    # Dry run of `prune`. Returns the number of members that
    # would be pruned without kicking anyone.
    def dry_prune(days : UInt32)
      @client.get_guild_prune_count(@id, days).pruned
    end

    # Prune users.
    #
    # Kicks all users whose last activity on the server
    # was more than *days* days ago.
    def prune(days : UInt32)
      @client.begin_guild_prune(@id, days).pruned
    end

    def create_role(name : String, perms : Permissions? = nil, color = 0_u32,
                    hoisted = false, mentionable = false)
      role = @client.create_guild_role(
        guild_id: @id,
        name: name,
        permissions: perms,
        colour: color,
        hoisted: hoisted,
        mentionable: mentionable
      )
      @role_ids << role.id
      role
    end

    def bans
      @client.get_guild_bans @id
    end

    def banned?(user : User)
      bans.includes? user
    end

    def name=(name : String)
      @client.modify_guild guild_id: @id, name: name
      @name = name
    end

    def region=(region : String)
      @client.modify_guild guild_id: @id, region: region
      @region = region
    end

    def verification_level=(verif_lvl : Guild::VerificationLevel)
      @client.modify_guild guild_id: @id, verif_lvl: verif_lvl
      @verification_level = verif_lvl
    end

    def afk_channel=(channel : GuildChannel)
      @client.modify_guild guild_id: @id, afk_channel_id: channel.id
      @afk_channel_id = channel.id
    end

    def afk_timeout=(seconds : Int32)
      @client.modify_guild guild_id: @id, afk_timeout: seconds
      @afk_timeout = seconds
    end

    def afk_timeout=(span : Time::Span)
      self.afk_timeout = span.seconds
    end

    def icon=(url : String)
      @client.modify_guild guild_id: @id, icon: url
      @icon = url
    end

    def owner=(user_or_member : User | GuildMember)
      @client.modify_guild guild_id: @id, owner_id: user_or_member.id
      @owner = user_or_member.id
    end

    def splash=(url : String)
      @client.modify_guild guild_id: @id, splash: url
      @splash = url
    end

    def delete
      @client.delete_guild @id
    end

    def channels
      @client.get_guild_channels @id
    end

    class ChannelNotFoundInGuildError < DCRError
      getter name : String

      def initialize(@name : String)
        super "This guild has no channel called '#{@name}'."
      end
    end

    # Tries to find a channel by name.
    #
    # The input argument can start with or without a hash. If no channel
    # with this *name* is found, `nil` is returned.
    def channel(name : String)
      name = name[1..-1] if name.starts_with? '#'
      channels.find { |c| c.name == name }
    end

    # Tries to find a channel by name.
    #
    # The input argument can start with or without a hash. If no channel
    # with this *name* is found, `ChannelNotFoundInGuildError` is raised.
    def channel!(name : String)
      channel || raise ChannelNotFoundInGuildError.new name
    end

    # Deletes this guild.
    #
    # Requires the **bot** to be an owner of the server.
    def delete
      @client.delete_guild @id
    end

    def create_text_channel(name : String)
      @channel.create_guild_channel(
        guild_id: @id,
        type: Channel::Type::GuildText,
        name: name
      ).as(GuildTextChannel)
    end

    def create_voice_channel(name : String)
      @channel.create_guild_channel(
        guild_id: @id,
        type: Channel::Type::GuildVoice,
        name: name
      ).as(GuildVoiceChannel)
    end

    # Lists members in this guild.
    #
    # The maximum *limit* possible is 1000.
    # You can use *after* to specify which messages should be listed.
    def list_members(limit : Int32 = 1000, after : GuildMember? = nil)
      @client.list_members guild_id: @id, limit: limit, after: after.id
    end
  end

  struct Guild
    include Payload
    include GuildCommon

    # :nodoc:
    def initialize(payload : Gateway::GuildCreatePayload)
      {% for ivar in @type.instance_vars %}
        {% if !{"embed_enabled", "embed_channel_id"}.includes? ivar.name.stringify %}
          @{{ivar.name}} = payload.@{{ivar.name}}
        {% end %}
      {% end %}
    end
  end

  struct PartialGuild
    include Payload

    field id : UInt64, converter: SnowflakeConverter
  end

  struct GuildEmbed
    include Payload

    field enabled? : Bool, key: "enabled"
    field channel_id : UInt64, converter: SnowflakeConverter
  end

  # A particular on a particular guild.
  struct GuildMember
    include Mentionable
    include Payload::WithClientAccess

    field user : User, pass_extra_fields: {:client}
    field nick : String? = nil
    field role_ids : Array(UInt64), key: "roles", converter: SnowflakeArrayConverter
    field joined_at : Time? = nil, converter: Time::Format::ISO_8601_DATE
    field deaf? : Bool = false, key: "deaf"
    field muted? : Bool = false, key: "mute"

    # :nodoc:
    def initialize(@client, member : GuildMember, update : Gateway::GuildMemberUpdatePayload)
      @user = update.@user
      @nick = update.@nick
      @role_ids = update.@role_ids
      @joined_at = member.@joined_at
      @deaf = member.@deaf
      @muted = member.@muted
      @guild_id = member.@guild_id
    end

    field guild_id : UInt64? = nil, getter: false, dont_deserialize: true, dont_serialize: true

    def owner?
      guild = @client.get_guild guild_id
      guild.owner_id == id
    end

    # :nodoc:
    def nick=(n)
      @nick = n
    end

    def guild_id
      @guild_id.as(UInt64)
    end

    # :nodoc:
    def guild_id=(id : UInt64)
      raise "Tried to set guild ID after it has already been set (note: this is an internal method!)" if !@guild_id.nil?
      @guild_id = id
    end

    def guild
      @client.get_guild guild_id
    end

    # Returns the effective display name of the member (i.e, the nick or
    # the username if the member doesn't have a nick).
    def display_name
      @nick || @user.username
    end

    def roles
      @role_ids.map { |id| @client.get_role id }
    end

    # Determines whether the member has certain permissions in the *channel*.
    def has_permissions_in?(channel : GuildChannel, perms : Permissions) : Bool
      channel.member_has_permissions? self, perms
    end

    # Determines whether the member can send messages in the *channel*.
    def can_send_in?(channel : GuildChannel) : Bool
      channel.member_can_sendin? self
    end

    # Determines whether the member can read messages from the *channel*.
    def can_read?(channel : GuildChannel) : Bool
      channel.member_can_read? self
    end

    def nick=(nick : String)
      @client.modify_guild_member guild_id: guild_id, user_id: id, nick: nick
      @nick = nick
    end

    def roles=(roles : Array(Role))
      ids = roles.map &.id
      @client.modify_guild_member guild_id: guild_id, user_id: id, roles: ids
      @role_ids = ids
    end

    def add_role(role : Role)
      @role_ids << role.id
      begin
        @client.modify_guild_member guild_id: guild_id, user_id: id, roles: @role_ids
      rescue e
        @role_ids.delete role.id
        raise e
      end
    end

    def remove_role(role : Role)
      has_role = @role_ids.includes? role.id
      if has_role
        @role_ids.delete role.id
        begin
          @client.modify_guild_member guild_id: guild_id, user_id: id, roles: @role_ids
        rescue e
          @role_ids << role.id
          raise e
        end
      end
      nil
    end

    def mute : Nil
      @client.modify_guild_member guild_id: guild_id, user_id: id, mute: true
      @muted = true
    end

    def deafen : Nil
      @client.modify_guild_member guild_id: guild_id, user_id: id, deaf: true
      @deaf = true
    end

    def unmute : Nil
      @client.modify_guild_member guild_id: guild_id, user_id: id, mute: false
      @muted = false
    end

    def undeafen : Nil
      @client.modify_guild_member guild_id: guild_id, user_id: id, deaf: false
      @deaf = false
    end

    def toggle_mute
      @client.modify_guild_member guild_id: guild_id, user_id: id, mute: !@muted
      @muted = !@muted
    end

    def toggle_deafen
      @client.modify_guild_member guild_id: guild_id, user_id: id, mute: !@deaf
      @deaf = !@deaf
    end

    # Moves the member to a voice channel.
    def move_to(voice_channel : GuildVoiceChannel) : Nil
      @client.modify_guild_member guild_id: guild_id, user_id: id, channel_id: voice_channel.id
    end

    def kick : Nil
      @client.remove_guild_member guild_id, id
    end

    def ban : Nil
      @client.create_guild_ban guild_id, id
    end

    def unban : Nil
      # if you for some reason keep around the guildmember struct...
      @client.remove_guild_ban guild_id, id
    end

    {% begin %}
      {% for method in User.methods %}
        {% if !{"initialize", "username=", "avatar=", "status=", "game="}.includes? method.name.stringify %}
          # See `User#{{method.name.id}}`.
          def {{method.name.id}}({{*method.args}})
            user.{{method.name.id}}(
              {% for arg in method.args %}
                {{arg.internal_name}},
              {% end %}
            )
          end
        {% elsif method.name.ends_with? "=" %}
          # See `User#{{method.name.id}}`.
          def {{method.name.id}}({{method.args[0]}})
            user.{{method.name.id}} {{method.args[0].internal_name}}
          end
        {% end %}
      {% end %}
      {% for k, v in User::MAGICJSON_FIELDS %}
        # See `User#{{k.id}}`.
        def {{k.id}}
          user.{{k.id}}
        end
      {% end %}
    {% end %}
  end

  struct Integration
    include Payload

    field id : UInt64, converter: SnowflakeConverter
    field name : String
    field type : String
    field enabled? : Bool, key: "enabled"
    field syncing? : Bool, key: "syncing"
    field role_id : UInt64, converter: SnowflakeConverter
    field expire_behavior : UInt8
    field expire_grace_period : Int32
    field user : User
    field account : IntegrationAccount
    field synced_at : Time, converter: Time::EpochConverter

    {% unless flag?(:murica) %}
      def expire_behaviuor
        expire_behavior
      end
    {% end %}
  end

  struct IntegrationAccount
    include Payload

    field id : String
    field name : String
  end

  struct Emoji
    include Payload

    field id : UInt64, converter: SnowflakeConverter
    field name : String
    field role_ids : Array(UInt64), key: "roles", converter: SnowflakeArrayConverter
    field requires_colons? : Bool, key: "require_colons"
    field managed? : Bool, key: "managed"
  end

  struct Role
    include Payload
    include Mentionable
    extend FromMentionable

    field id : UInt64, converter: SnowflakeConverter
    field name : String
    field permissions : Permissions
    field color : UInt32
    field hoisted? : Bool, key: "hoist"
    field position : Int32
    field managed? : Bool, key: "managed"
    field mentionable? : Bool, key: "mentionable"

    {% unless flag?(:murica) %}
      def colour
        color
      end
    {% end %}

    field guild_id : UInt64? = nil, getter: false, dont_deserialize: true, dont_serialize: true

    def guild_id
      @guild_id.as(UInt64)
    end

    # :nodoc:
    def guild_id=(id : UInt64)
      raise "Tried to set guild ID after it has already been set (note: this is an internal method!)" if !@guild_id.nil?
      @guild_id = id
    end

    def name=(name : String)
      @client.modify_guild_role(
        guild_id: guild_id,
        role_id: @id,
        name: name,
        permissions: @permissions
      )
      @name = name
    end

    def permissions=(permissions : Permissions)
      @client.modify_guild_role(
        guild_id: guild_id,
        role_id: @id,
        permissions: permissions
      )
      @permissions = permissions
    end

    def color=(color : UInt32)
      @client.modify_guild_role(
        guild_id: guild_id,
        role_id: @id,
        colour: color,
        permissions: permissions
      )
      @color = color
    end

    def position=(pos : Int32)
      @client.modify_guild_role(
        guild_id: guild_id,
        role_id: @id,
        position: pos,
        permissions: permissions
      )
      @position = pos
    end

    def hoisted?=(hoisted : Bool)
      @client.modify_guild_role(
        guild_id: guild_id,
        role_id: @id,
        hoist: hoisted,
        permissions: permissions
      )
      @hoisted = hoisted
    end

    def delete
      @client.delete_guild_role guild_id: guild_id, role_id: @id
    end

    {% unless flag?(:murica) %}
      def colour=(colour : UInt32)
        self.color = colour
      end
    {% end %}

    protected def self.mention_prefixes
      {"@&"}
    end

    protected def self.resolve_from_id(client, id)
      client.get_role id
    end

    def mention(io)
      io << "<@&" << id << ">"
    end
  end
end
