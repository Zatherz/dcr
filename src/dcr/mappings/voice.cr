module Discord
  struct VoiceState
    include Payload::WithClientAccess

    field guild_id : UInt64? = nil, converter: MaybeSnowflakeConverter
    field channel_id : UInt64? = nil, converter: MaybeSnowflakeConverter
    field user_id : UInt64, converter: SnowflakeConverter
    field session_id : String
    field deaf? : Bool, key: "deaf"
    field muted? : Bool, key: "mute"
    field self_deaf? : Bool, key: "self_deaf"
    field self_muted? : Bool, key: "self_mute"
    field suppressed? : Bool, key: "suppress"

    class NoGuildError < DCRError
      def initialize
        super "This voice state does not have an associated guild."
      end
    end

    class NoChannelError < DCRError
      def initialize
        super "This voice state does not have an associated channel."
      end
    end

    def guild : Guild?
      if _id = @guild_id
        return @client.get_guild _id
      end
      nil
    end

    def guild! : Guild
      guild || raise NoGuildError.new
    end

    def channel : Channel?
      if _id = @channel_id
        return @client.get_channel _id
      end
      nil
    end

    def channel! : Channel
      channel || raise NoChannelError.new
    end

    def user : User
      @client.get_user @user_id
    end
  end

  struct VoiceRegion
    include Payload

    field id : String
    field name : String
    field sample_hostname : String
    field sample_port : UInt16
    field custom? : Bool = false, key: "custom"
    field vip? : Bool, key: "vip"
    field optimal? : Bool, key: "optimal"
  end
end
