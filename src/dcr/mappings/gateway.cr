require "./user"
require "./channel"
require "./guild"

module Discord
  module Gateway
    struct ReadyPayload
      include Payload::WithClientAccess

      field version : UInt8, key: "v"
      field user : User, pass_extra_fields: {:client}
      field private_channels : Array(PrivateChannel), converter: {
        type:              TypedArrayConverter(PrivateChannel),
        pass_extra_fields: {:client},
      }
      field guilds : Array(PartialGuild)
      field session_id : String
    end

    struct ResumedPayload
      include Payload

      field trace : Array(String), key: "_trace"
    end

    struct IdentifyPacket
      include Payload

      def initialize(token, properties, large_threshold, compress, shard)
        @op = Discord::Client::OP_IDENTIFY
        @d = IdentifyPayload.new(token, properties, large_threshold, compress, shard)
      end

      field op : Int32
      field d : IdentifyPayload
    end

    struct IdentifyPayload
      include Payload

      def initialize(@token, @properties, @compress, @large_threshold, @shard)
      end

      field token : String
      field properties : IdentifyProperties
      field compress? : Bool, key: "compress"
      field large_threshold : Int32
      field shard : {Int32, Int32}?
    end

    struct IdentifyProperties
      include Payload

      def initialize(@os, @browser, @device, @referrer, @referring_domain)
      end

      field os : String, key: "$os"
      field browser : String, key: "$browser"
      field device : String, key: "$device"
      field referrer : String, key: "$referrer"
      field referring_domain : String, key: "$referring_domain"
    end

    struct ResumePacket
      include Payload

      def initialize(token, session_id, seq)
        @op = Discord::Client::OP_RESUME
        @d = ResumePayload.new(token, session_id, seq)
      end

      field op : Int32
      field d : ResumePayload
    end

    # :nodoc:
    struct ResumePayload
      include Payload

      def initialize(@token, @session_id, @seq)
      end

      field token : String
      field session_id : String
      field seq : Int64
    end

    struct StatusUpdatePacket
      include Payload

      def initialize(status, game, afk, since)
        @op = Discord::Client::OP_STATUS_UPDATE
        @d = StatusUpdatePayload.new(status, game, afk, since)
      end

      field op : Int32
      field d : StatusUpdatePayload
    end

    # :nodoc:
    struct StatusUpdatePayload
      include Payload

      def initialize(@status, @game, @afk, @since)
      end

      field status : String? = nil, emit_null: true
      field game : GamePlaying? = nil, emit_null: true
      field afk? : Bool, key: "afk"
      field since : Int64
    end

    struct VoiceStateUpdatePacket
      include Payload

      def initialize(guild_id, channel_id, self_mute, self_deaf)
        @op = Discord::Client::OP_VOICE_STATE_UPDATE
        @d = VoiceStateUpdatePayload.new(guild_id, channel_id, self_mute, self_deaf)
      end

      field op : Int32
      field d : VoiceStateUpdatePayload
    end

    # :nodoc:
    struct VoiceStateUpdatePayload
      include Payload

      def initialize(@guild_id, @channel_id, @self_muted, @self_deaf)
      end

      field guild_id : UInt64
      field channel_id : UInt64? = nil, emit_null: true
      field self_muted? : Bool, key: "self_mute"
      field self_deaf? : Bool, key: "self_deaf"
    end

    struct RequestGuildMembersPacket
      include Payload

      def initialize(guild_id, query, limit)
        @op = Discord::Client::OP_REQUEST_GUILD_MEMBERS
        @d = RequestGuildMembersPayload.new(guild_id, query, limit)
      end

      field op : Int32
      field d : RequestGuildMembersPayload
    end

    # :nodoc:
    struct RequestGuildMembersPayload
      include Payload

      def initialize(@guild_id, @query, @limit)
      end

      field guild_id : UInt64, converter: SnowflakeConverter
      field query : String
      field limit : Int32
    end

    struct HelloPayload
      include Payload

      field heartbeat_interval : UInt32
      field trace : Array(String), key: "_trace"
    end

    # This one is special from simply Guild since it also has fields for members
    # and presences.
    struct GuildCreatePayload
      include Payload::WithClientAccess
      include GuildCommon

      field large? : Bool, key: "large"
      field voice_states : Array(VoiceState), pass_extra_fields: {:client}
      field was_unavailable? : Bool = false, key: "unavailable"
      field member_count : Int32
      field roles : Array(Role)
      field members : Array(GuildMember), pass_extra_fields: {:client}
      field channels : Array(GuildChannel), converter: {
        type:              TypedArrayConverter(GuildChannel),
        pass_extra_fields: {:client},
      }
      field presences : Array(Presence), converter: {
        type:              ArrayConverter(Presence),
        pass_extra_fields: {:client},
      }

      # :nodoc:
      def channels=(ary : Array(GuildChannel))
        @channels = ary
      end

      # :nodoc:
      def members=(ary : Array(GuildMember))
        @members = ary
      end

      # :nodoc:
      def roles=(ary : Array(Role))
        @roles = ary
      end
    end

    struct GuildDeletePayload
      include Payload

      field id : UInt64, converter: SnowflakeConverter
      field unavailable? : Bool = false, key: "unavailable"
    end

    struct GuildBanPayload
      include Payload

      field id : UInt64, converter: SnowflakeConverter
      field username : String
      field discriminator : String
      field avatar : String? = nil
      field bot? : Bool = false, key: "bot"
      field guild_id : UInt64, converter: SnowflakeConverter
    end

    struct GuildEmojiUpdatePayload
      include Payload

      field guild_id : UInt64, converter: SnowflakeConverter
      field emojis : Array(Emoji)

      {% unless flag?(:murica) %}
        def emoji
          emojis
        end
      {% end %}
    end

    struct GuildIntegrationsUpdatePayload
      include Payload

      field guild_id : UInt64, converter: SnowflakeConverter
    end

    struct GuildMemberAddPayload
      include Payload

      # rest of this is GuildMember
      field guild_id : UInt64, converter: SnowflakeConverter
    end

    struct GuildMemberUpdatePayload
      include Payload::WithClientAccess

      field user : User, pass_extra_fields: {:client}
      field nick : String? = nil
      field role_ids : Array(UInt64), key: "roles", converter: SnowflakeArrayConverter
      field guild_id : UInt64, converter: SnowflakeConverter
    end

    struct GuildMemberRemovePayload
      include Payload::WithClientAccess

      field user : User, pass_extra_fields: {:client}
      field guild_id : UInt64, converter: SnowflakeConverter
    end

    struct GuildMembersChunkPayload
      include Payload::WithClientAccess

      field guild_id : UInt64, converter: SnowflakeConverter
      field members : Array(GuildMember), pass_extra_fields: {:client}
    end

    struct GuildRolePayload
      include Payload

      field guild_id : UInt64, converter: SnowflakeConverter
      field role : Role
    end

    struct GuildRoleDeletePayload
      include Payload

      field guild_id : UInt64, converter: SnowflakeConverter
      field role_id : UInt64, converter: SnowflakeConverter
    end

    struct MessageReactionRemoveAllPayload
      include Payload

      field channel_id : UInt64, converter: SnowflakeConverter
      field message_id : UInt64, converter: SnowflakeConverter
    end

    struct MessageUpdatePayload
      include Payload::WithClientAccess

      field type : Message::Type? = nil
      field content : String? = nil
      field id : UInt64, converter: SnowflakeConverter
      field channel_id : UInt64, converter: SnowflakeConverter
      field author : User? = nil, pass_extra_fields: {:client}
      field timestamp : Time? = nil, converter: DATE_FORMAT
      field tts? : Bool = false, key: "tts"
      field mentions_everyone? : Bool = false, key: "mention_everyone"
      field mentions : Array(User)? = nil, pass_extra_fields: {:client}
      field mentioned_role_ids : Array(UInt64)? = nil, key: "mention_roles", converter: SnowflakeArrayConverter
      field attachments : Array(Attachment)? = nil
      field embeds : Array(Embed)? = nil
      field pinned? : Bool = false, key: "pinned"
    end

    struct MessageDeletePayload
      include Payload

      field id : UInt64, converter: SnowflakeConverter
      field channel_id : UInt64, converter: SnowflakeConverter
    end

    struct MessageDeleteBulkPayload
      include Payload

      field ids : Array(UInt64), converter: SnowflakeArrayConverter
      field channel_id : UInt64, converter: SnowflakeConverter
    end

    struct ChannelPinsUpdatePayload
      include Payload

      field last_pin_timestamp : Time, converter: TimestampConverter
      field channel_id : UInt64, converter: SnowflakeConverter
    end

    struct PresenceUpdatePayload
      include Payload::WithClientAccess

      field id : UInt64, key: "user", converter: PresenceIDConverter
      field role_ids : Array(UInt64), key: "roles", converter: SnowflakeArrayConverter
      field game : GamePlaying? = nil
      field nick : String? = nil
      field guild_id : UInt64, converter: SnowflakeConverter
      field status : Presence::Status, converter: StringEnumConverter(Presence::Status)
    end

    struct TypingStartPayload
      include Payload

      field channel_id : UInt64, converter: SnowflakeConverter
      field user_id : UInt64, converter: SnowflakeConverter
      field timestamp : Time, converter: Time::EpochConverter
    end

    struct VoiceServerUpdatePayload
      include Payload

      field token : String
      field guild_id : UInt64, converter: SnowflakeConverter
      field endpoint : String
    end
  end
end
