module Discord
  module REST
    # A response to the Get Gateway REST API call.
    struct GatewayResponse
      include Payload

      field url : String
    end

    # A response to the Get Guild Prune Count REST API call.
    struct PruneCountResponse
      include Payload

      field pruned : UInt32
    end

    # A payload for the Modify Channel REST API call.
    #
    # Used instead of a tuple to control null emitting.
    struct ModifyChannelPayload
      include Payload

      field name : String? = nil
      field position : UInt32? = nil
      field topic : String? = nil
      field bitrate : UInt32? = nil
      field user_limit : UInt32? = nil

      def initialize(@name, @position, @topic, @bitrate, @user_limit)
      end
    end

    # A payload for the Modify Guild REST API call.
    #
    # Used instead of a tuple to control null emitting.
    struct ModifyGuildPayload
      include Payload

      field name : String? = nil
      field region : String? = nil
      field verification_level : UInt8? = nil
      field afk_channel_id : UInt64? = nil
      field afk_timeout : Int32? = nil
      field icon : String? = nil
      field owner_id : UInt64? = nil
      field splash : String? = nil

      def initialize(@name, @region, @verification_level, @afk_channel_id,
                     @afk_timeout, @icon, @owner_id, @splash)
      end
    end

    # A payload for the Modify Guild Member REST API call.
    #
    # Used instead of a tuple to control null emitting.
    struct ModifyGuildMemberPayload
      include Payload

      field nick : String? = nil
      field roles : Array(UInt64)? = nil
      field mute : Bool? = nil
      field deaf : Bool? = nil
      field channel_id : UInt64? = nil

      def initialize(@nick, @roles, @mute, @deaf, @channel_id)
      end
    end

    # A payload for the Modify Guild Role REST API call.
    #
    # Used instead of a tuple to control null emitting.
    struct ModifyGuildRolePayload
      include Payload

      field name : String? = nil
      field permissions : Permissions
      field colour : UInt32? = nil
      field position : Int32? = nil
      field hoist : Bool? = nil

      def initialize(@name, @permissions, @colour, @position, @hoist)
      end
    end

    # A payload for the Modify Current User REST API call.
    #
    # Used instead of a tuple to control null emitting.
    struct ModifyCurrentUserPayload
      include Payload

      field username : String? = nil
      field avatar : String? = nil

      def initialize(@username, @avatar)
      end
    end
  end
end
