require "./client"

module Discord
  # CachedClient is an extension of `Client` which adds
  # caching functionality.
  #
  # CachedClient reduces the load on Discord's servers
  # and latency caused by API calls. It does this by storing
  # payloads in hashes with their IDs as keys.
  #
  # Thanks to dcr's design, CachedClient nicely integrates
  # into the API of Client. It basically redefines
  # some REST calls to retrieve from cache if available,
  # instead of doing a request. Even when a request is
  # necessary, it'll execute it, and then save the result of
  # it. It also adds some event handlers responsible for
  # caching the data provided through the websocket interface.
  #
  # This is also why CachedClient is recommended for actual
  # bots which are going to be using the gateway. If you don't
  # use the gateway, the cached data might go out of date quickly.
  # However, if you use a gateway, you will receive update events
  # which forcefully refresh the cached object with new content.
  #
  # An example CachedClient instantiation looks like this:
  #
  # ```
  # client = CachedClient.new token: "Bot ABCDEFGHIJKLMNOPQRSTUVWXYZ"
  # ```
  class CachedClient < Client
    @current_user : User? = nil
    @current_presence : Presence? = nil

    def initialize(*args, **kwargs)
      super *args, **kwargs
      @users = Hash(UInt64, User).new
      @channels = Hash(UInt64, Channel).new
      @guilds = Hash(UInt64, Guild).new
      @members = Hash(UInt64, Hash(UInt64, GuildMember)).new
      @roles = Hash(UInt64, Role).new
      @presences = Hash(UInt64, Presence).new
      # @messages = Hash(UInt64, Message).new

      @dm_channels = Hash(UInt64, UInt64).new

      @guild_roles = Hash(UInt64, Set(UInt64)).new
      @guild_channels = Hash(UInt64, Set(UInt64)).new

      initialize_cache_events
    end

    def initialize_cache_events
      on_ready { |r| cache r }

      on_channel_create { |c| cache c }
      on_channel_update { |c| cache c }
      on_channel_delete { |c| delete c }

      on_guild_create { |gc| cache gc }
      on_guild_update { |g| cache g }
      on_guild_delete { |gd| delete gd }

      on_guild_member_add { |m| cache m }
      on_guild_member_update { |m| cache m }
      on_guild_member_remove { |mr| delete mr }
      on_guild_ban_add { |ba| delete_member ba.guild_id, ba.id }

      on_guild_role_create { |r| cache r }
      on_guild_role_update { |r| cache r }
      on_guild_role_delete { |rd| delete rd }

      on_presence_update { |pu| cache pu }
      on_channel_pins_update { |cpu| cache cpu }
    end

    private def cache_current_user(user : User)
      @current_user = user
    end

    private def cache(ready_payload payload : Gateway::ReadyPayload)
      payload.private_channels.each do |channel|
        if channel.is_a? PrivateDMChannel
          recipient_id = channel.recipient.id
          cache_dm_channel recipient_id, channel.id
        end
      end
    end

    private def cache(channel : Channel)
      @channels[channel.id] = channel
      if channel.is_a? GuildChannel
        @guild_channels[channel.guild_id] ||= Set(UInt64).new
        @guild_channels[channel.guild_id] << channel.id
      elsif channel.is_a? PrivateDMChannel
        @dm_channels[channel.recipient.id] = channel.id
      end
    end

    private def cache(role : Role, guild_id : UInt64)
      @roles[role.id] = role
      @guild_roles[guild_id] ||= Set(UInt64).new
      @guild_roles[guild_id] << role
    end

    private def cache(guild_create : Gateway::GuildCreatePayload)
      guild_create.channels.each { |c| cache c }
      guild_create.roles.each { |r| cache r }
      guild_create.presences.each { |p| cache p }
      guild_create.members.each do |m|
        if !@presences[m.id]?
          @presences[m.id] = Presence.new(
            client: self, id: m.id,
            game: nil, status: Presence::Status::Offline
          )
        end
      end
      cache Guild.new(guild_create)
    end

    private def cache(guild : Guild)
      @guilds[guild.id] = guild
    end

    private def cache(guild_member payload : GuildMember)
      @members[payload.guild_id] ||= Hash(UInt64, GuildMember).new
      @members[payload.guild_id][payload.user.id] = payload
    end

    private def cache(guild_role_payload payload : Gateway::GuildRolePayload)
      @guild_roles[payload.guild_id] ||= Set(UInt64).new
      @guild_roles[payload.guild_id] << payload.role.id
      cache role: payload.role
    end

    private def cache(role : Role)
      @roles[role.id] = role
    end

    private def cache(channel_pins_update_payload payload : Gateway::ChannelPinsUpdatePayload)
      channel = get_channel(payload.channel_id).as(TextChannel)
      channel.last_pin_timestamp = payload.last_pin_timestamp
      @channels[channel.id] = channel
    end

    private def cache(presence payload : Presence)
      @presences[payload.id] = payload
    end

    private def cache(presence_update_payload payload : Gateway::PresenceUpdatePayload)
      if payload.nick
        if member = @members[payload.guild_id]?.try &.[payload.id]?
          member.nick = payload.nick
        end
      end

      @presences[payload.id] = Presence.new(self, payload)

      # TODO: roles? @member_roles? not sure if worth
    end

    private def cache_dm_channel(recipient_id : UInt64, channel_id : UInt64)
      @dm_channels[recipient_id] = channel_id
    end

    private def delete(channel : Channel)
      @channels.delete channel
      if channel.is_a? GuildChannel
        @guild_channels[channel.guild_id]?.try &.delete channel.id
      elsif channel.is_a? PrivateDMChannel
        @dm_channels.delete channel.recipient.id
      end
    end

    private def delete(guild_member_remove_payload payload : Gateway::GuildMemberRemovePayload)
      delete_member payload.guild_id, payload.user.id
    end

    private def delete(guild_role_delete_payload payload : Gateway::GuildRoleDeletePayload)
      @roles.delete payload.role_id
      @guild_roles[payload.guild_id]?.try &.delete payload.role_id
    end

    private def delete(guild_delete_payload payload : Gateway::GuildDeletePayload)
      @guilds.delete payload.id
    end

    private def delete_member(guild_id : UInt64, user_id : UInt64)
      @members[guild_id]?.try &.delete user_id
    end

    private def delete_guild(id : UInt64)
      @guilds.delete id
    end

    class FailedRetrievingError < DCRError
      def initialize(msg)
        super msg
      end
    end

    # @presences

    def get_presence(user_id : UInt64)
      presence = @presences[user_id]?
      raise FailedRetrievingError.new "Presence for user #{user_id} hasn't been cached (this shouldn't happen)." if presence.nil?
      presence.not_nil!
    end

    # @channels / @guild_channels

    def get_channel(channel_id : UInt64)
      @channels.fetch(channel_id) { @channels[channel_id] = super }
    end

    def modify_channel(channel_id : UInt64, name : String?, position : UInt32?,
                       topic : String?, bitrate : UInt32?, user_limit : UInt32?)
      super.tap do |chan|
        @channels[channel_id] = chan
        if chan.is_a? GuildChannel
          @guild_channels[chan.guild_id] ||= Set(UInt64).new
          @guild_channels[chan.guild_id] << chan.id
        end
      end
    end

    def delete_channel(channel_id : UInt64) : Nil
      if chan = @channels[channel_id]?
        if chan.is_a? GuildChannel
          @guild_channels[chan.guild_id]?.try &.delete channel_id
        end
      end
      @channels.delete channel_id
      super
    end

    def create_guild_channel(guild_id : UInt64, name : String, type : UInt8,
                             bitrate : UInt32?, user_limit : UInt32?)
      @guild_channels[guild_id] ||= [] of Channels
      super.tap do |c|
        @guild_channels[guild_id] << c
        @channels << c
      end
    end

    def modify_guild_channel(guild_id : UInt64, channel_id : UInt64,
                             position : UInt64)
      @guild_channels[guild_id] ||= [] of Channels
      super.tap { |c| @guild_channels[guild_id] << c }
    end

    def get_guild_channels(guild_id : UInt64)
      @guild_channels.fetch(guild_id) { @guild_channels[guild_id] = super }
    end

    # @messages

    # def get_channel_messages(channel_id : UInt64, limit : UInt8 = 50,
    #                          before : UInt64? = nil, after : UInt64? = nil,
    #                          around : UInt64? = nil)
    #   # effectively impossible to fetch this so we have to make a request
    #   # every single time
    #   super.tap &.each { |m| @messages[m.id] = m }
    # end

    # def get_channel_message(channel_id : UInt64, message_id : UInt64)
    #   @messages.fetch(message_id) { @messages[message_id] = super }
    # end

    # def create_message(*args, **kwargs)
    #   super.tap { |m| @messages[m.id] = m }
    # end

    # def edit_message(channel_id : UInt64, message_id : UInt64, content : String, embed : Embed? = nil)
    #   super.tap { |m| @messages[m.id] = m }
    # end

    # def delete_message(channel_id : UInt64, message_id : UInt64)
    #   @messages.delete message_id
    #   super
    # end

    # def bulk_delete_messages(channel_id : UInt64, message_ids : Array(UInt64))
    #   message_ids.each { @messages.delete message_ids }
    #   super
    # end

    # @guilds

    def get_guild(guild_id : UInt64)
      @guilds.fetch(guild_id) { @guilds[guild_id] = super }
    end

    def modify_guild(guild_id : UInt64, name : String?, region : String?,
                     verification_level : UInt8?, afk_channel_id : UInt64?,
                     afk_timeout : Int32?, icon : String?, owner_id : UInt64?,
                     splash : String?)
      @guilds[guild_id] = super
    end

    def delete_guild(guild_id : UInt64)
      @guilds.delete guild_id
      super
    end

    # @members

    def list_members(guild_id : UInt64, limit : UInt8 = 1, after : UInt64 = 0)
      if @members.has_key? guild_id
        @members[guild_id].values
      else
        super.tap do |members|
          h = @members[guild_id] = Hash(UInt64, Member).new
          members.each { |m| h[m.id] = m }
        end
      end
    end

    def modify_guild_member(guild_id : UInt64, user_id : UInt64, nick : String?,
                            roles : Array(UInt64)?, mute : Bool?, deaf : Bool?,
                            channel_id : UInt64?)
      @members[guild_id] ||= Hash(UInt64, Member).new
      @members[guild_id][user_id] = super
    end

    def remove_guild_member(guild_id : UInt64, user_id : UInt64)
      @members[guild_id]?.try &.delete user_id
    end

    # @roles

    def get_guild_roles(guild_id : UInt64)
      super.tap { |roles| roles.each { |r| @roles[r.id] = r } }
    end

    def create_guild_role(guild_id : UInt64, name : String? = nil,
                          permissions : Permissions? = nil, colour : UInt32 = 0,
                          hoist : Bool = false, mentionable : Bool = false)
      super.tap { |r| @roles[r.id] = r }
    end

    def modify_guild_role(guild_id : UInt64, role_id : UInt64, name : String?,
                          permissions : Permissions, colour : UInt32?,
                          position : Int32?, hoist : Bool?)
      @roles[role_id] = super
    end

    def delete_guild_role(guild_id : UInt64, role_id : UInt64)
      @roles.delete role_id
    end

    def get_role(role_id : UInt64)
      # ONLY IN CACHEDCLIENT!!!
      role = @roles[role_id]?
      raise FailedRetrievingError.new "Role #{role_id} doesn't exist, is in a server you aren't in, or hasn't been cached." if role.nil?
      role.not_nil!
    end

    # @current_user / @current_presence

    def get_current_user
      @current_user ||= super
    end

    def modify_current_user(username : String, avatar : String)
      @current_user = super
    end

    def get_current_presence
      @current_presence ||= Presence.new(
        user: PartialUser.new(get_current_user),
        status: Presence::Status::Online,
        game: nil
      )
    end

    # @users
    def get_user(user_id : UInt64)
      @users.fetch(user_id) { @users[user_id] = super }
    end

    def query_users(query : String, limit : Int32 = 25)
      super.tap &.each { |u| @users[u.id] = u }
    end

    # @dm_channels

    def create_dm(recipient_id : UInt64)
      dm = super
      @dm_channels[recipient_id] = dm.id
      @channels[dm.id] = dm
      dm
    end

    def get_user_dms
      super.tap &.each { |c| @dm_channels[c.recipient.id] = c.id }
    end

    # other stuff

    def status_update(status : Presence::Status? = nil, game : GamePlaying? = nil, afk : Bool = false, since : Int64 = 0_i64)
      result = super

      prev_status = Presence::Status::Online # default
      if pres = @current_presence
        prev_status = pres.status
      end

      @current_presence = Presence.new(
        user: PartialUser.new(get_current_user),
        status: status || prev_status,
        game: game
      )
      result
    end
  end
end
